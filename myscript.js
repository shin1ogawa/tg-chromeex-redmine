(function () {

	var topmenuDiv = $('div#wrapper div#wrapper2 div#top-menu');
	var headerDiv = $('div#wrapper div#wrapper2 div#header');
	var sidebarDiv = $('div#wrapper div#wrapper2 div#main div#sidebar');
	var contentDiv = $('div#wrapper div#wrapper2 div#main div#content');
	if (topmenuDiv.length !== 1
		|| headerDiv.length !== 1
		|| sidebarDiv.length !== 1
		|| contentDiv.length !== 1) {
		return;
	}

	$('body').append('<style type="text/css">' +
		'tr.current-row td:first-child { border-left: 2px #4d90f0 solid; }\n' +
		'tr.current-row { background-color: #ffd; }\n' +
		'tr.hided-row { display: none; }\n' +
		'div.help-window { display: none; overflow: hidden; opacity: .85; border-radius: 10px; ' +
		'background: #222 none repeat scroll 0; font-size: 12pt; ' +
		'left: 20%; width: 60%; top: 15%; z-index: 10000; color: white; position: fixed;}\n' +
		'div.help-window h3 { margin-left: 16pt; color: white; font-weight: bolder; font-size: 16pt; }' +
		'div.help-window h4 { margin-left: 16pt; color: white; font-weight: bolder; font-size: 14pt; }' +
		'div.help-window span.keybind { font-weight: bolder; color: yellow; }' +
		'</style>');
	$('body').append('<div class="help-window">' +
		'<div style="text-align:right;"><h3><span class="keybind">press esc</span> <span>to close this help</span></h3></div>' +
		'<h3>keybind</h3>' +
		'<h4>list of issues</h4><ul>' +
		'<li><span class="keybind">j/k, n/p</span> <span>move up/down current row</span></li>' +
		'<li><span class="keybind">o</span> <span>open issue on current row</span></li>' +
		'<li><span class="keybind">i</span> <span>filter by assigned to me</span></li>' +
		'<li><span class="keybind">t</span> <span>filter by created by me</span></li>' +
		'<li><span class="keybind">x</span> <span>toggle cheking on current row</span></li>' +
		'<li><span class="keybind">0</span> <span>reset filtering and current row</span></li>' +
		'<li><span class="keybind">c</span> <span>show alert to copy issue id and title(current row/selected rows)</span></li>' +
		'<li><span class="keybind">shift+c</span> <span>open window to create new issue</span></li>' +
		'<li><span class="keybind">,</span> <span>toggle sidebar</span></li>' +
		'</ul><h4>detail of issue</h4><ul>' +
		'<li><span class="keybind">u</span> <span>back to list of issues</span></li>' +
		'<li><span class="keybind">c</span> <span>show alert to copy issue id and title</span></li>' +
		'<li><span class="keybind">,</span> <span>toggle sidebar</span></li>' +
		'</ul></div>')

	var sidebarInitialWidth = '25%';//sidebarDiv.css('width');
	var contentInitialWidth = '73%';//contentDiv.css('width');
	var sidebarHidden = false;

	var fnToggleSidebar = function (event) {

		var fnHideSidebar = function () {
			sidebarHidden = true;
			sidebarDiv.fadeOut(200);
			sidebarDiv.css('width', '0%');
			contentDiv.css('width', '98%');
		};

		var fnShowSidebar = function () {
			sidebarHidden = false;
			sidebarDiv.css('width', sidebarInitialWidth);
			contentDiv.css('width', contentInitialWidth);
			sidebarDiv.fadeIn(200);
		};

		if (sidebarHidden) {
			fnShowSidebar();
		} else {
			fnHideSidebar();
		}
	};

	var list = $('tr.issue').length > 0;
	var detail = $('title').text().match(/.*\ - .*\#[0-9]+.*\- Redmine/) !== null;

	var fnCopyTitleWithURL = function (event) {
		var text = '';
		if (detail === true) {
			var match = $('title').text().match(/.*\ - (.*\#[0-9]+.*)\- Redmine/);
			if (match.length < 2) return;
			text = match[1] + '\n' + location.href;
		} else if (list) {
			var checked = $('tr.issue td input:checked');
			if (checked.length === 0) {
				if (currentRow === null) {
					return;
				} else {
					text = '#' + $('td.id a', currentRow).text() + ': ' + $('td.subject a', currentRow).text();
				}
			} else {
				$.each(checked, function (_, checkbox) {
					var tr = $(checkbox).parent().parent();
					text += '#' + $('td.id a', tr).text() + ': ' + $('td.subject a', tr).text() + '\n';
				});
			}
		}
		console.log(text);
		alert('please copy belows:\n' + text);
	};

	var currentRowIndex = -1, currentRow = null, rows = $('tr.issue'), rowMax = list ? rows.length : 0;
//	console.log('detail=' + detail + ', list=' + list + ', rowMax=' + rowMax);
	var fnMoveCurrent = function () {
		if (currentRow !== null) currentRow.removeClass('current-row');
		currentRow = $(rows[currentRowIndex]);
		currentRow.addClass('current-row');
	};
	var fnDownCurrent = function (event, amount) {
		if (list === false) return;
		currentRowIndex += amount;
		if (currentRowIndex >= rowMax) currentRowIndex = rowMax - 1;
		fnMoveCurrent();
	};
	var fnUpCurrent = function (event, amount) {
		if (list === false) return;
		currentRowIndex -= amount;
		if (currentRowIndex < 0) currentRowIndex = 0;
		fnMoveCurrent();
	};
	var fnCheckRow = function (event) {
		if (list === false) return;
		if (currentRow === null) return;
		$('input[type="checkbox"]', currentRow).trigger('click');
	};
	var fnOpenRow = function (event) {
		if (list === false) return;
		if (currentRow === null) return;
		var checked = $('tr.issue td input:checked');
		if (checked.length === 0) {
			location.href = $('td.id a', currentRow).attr('href');
		} else {
			$.each(checked, function (_, checkbox) {
				var tr = $(checkbox).parent().parent();
				window.open($('td.id a', tr).attr('href'));
			});
		}
	};
	var fnOpenIssues = function (event) {
		if (list === true) {
			fnResetState(event);
			return;
		}
		location.href = $('a.issues').attr('href');
	};
	var fnRefreshRows = function (selectorToShow) {
		if (currentRow) currentRow.removeClass('current-row');
		rows = $(selectorToShow);
		rowMax = rows.length;
		currentRowIndex = rowMax > 0 ? 0 : -1;
		currentRow = rowMax > 0 ? $(rows[0]) : null;
		if (rowMax === 0) return;
		currentRow.addClass('current-row');
	};
	var fnFilterAssignedToMe = function (event) {
		$('tr.issue').addClass('hided-row');
		$('tr.assigned-to-me').removeClass('hided-row');
		fnRefreshRows('tr.assigned-to-me');
	};
	var fnFilterCreatedToMe = function (event) {
		$('tr.issue').addClass('hided-row');
		$('tr.created-by-me').removeClass('hided-row');
		fnRefreshRows('tr.created-by-me');
	};
	var fnResetState = function (event) {
		$('h1').trigger('click'); // cancel checked
		$('tr.issue').removeClass('hided-row'); // cancel filtered
		fnRefreshRows('tr.issue');
	};

	$('body').bind('keydown', function (event) {
		if (event['metaKey'] === true || event['ctrlKey'] === true) {
			return true;
		}
		var src = event['srcElement'];
		if (src instanceof HTMLTextAreaElement
			|| src instanceof HTMLSelectElement
			|| src instanceof HTMLInputElement) {
			return true;
		}
		var code = event['keyCode'];
		if (code === 27 /*'escape'*/) {
			$('div.help-window').hide(100);
		} else if (code === 48 /*'0'*/) {
			fnResetState(event);
		} else if (code === 67 /*'c'*/) {
			if (event['shiftKey'] === false) {
				fnCopyTitleWithURL(event);
			} else {
				window.open($('a.new-issue').attr('href'));
			}
		} else if (code === 73 /*'i'*/) {
			fnFilterAssignedToMe(event);
		} else if (code === 74 /*'j'*/) {
			fnDownCurrent(event, 1);
		} else if (code === 75 /*'k'*/) {
			fnUpCurrent(event, 1);
		} else if (code === 84 /*'t'*/) {
			fnFilterCreatedToMe(event);
		} else if (code === 88 /*'x'*/) {
			fnCheckRow(event);
		} else if (code === 78 /*'n'*/) {
			fnDownCurrent(event, 2);
		} else if (code === 79 /*'o'*/) {
			fnOpenRow(event);
		} else if (code === 80 /*'p'*/) {
			fnUpCurrent(event, 2);
		} else if (code === 85 /*'u'*/) {
			fnOpenIssues(event);
		} else if (event['shiftKey'] === true && code === 191 /*'?'*/) {
			$('div.help-window').fadeIn(100);
		} else if (code === 121 /*'f10'*/) {
			var checked = $('tr.issue td input:checked');
			if (checked.length === 0) return;
			$(checked[0]).trigger('contextmenu'); // FIXME show contextmenu does not work !
		} else if (code === 188 /*'comma'*/) {
			fnToggleSidebar(event);
		}
	});
	fnToggleSidebar();
}());
